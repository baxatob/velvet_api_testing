gabi:
	python -W ignore:InsecureRequestWarning -W ignore:ResourceWarning -m myrunner g_test_launcher
try:
	python -W ignore:InsecureRequestWarning -m myrunner g_test_single
lint:
	python -m flake8 handlers.py
