import os
from gabbi import driver
from utils.configuration import HOST, PORT


TESTS_DIR = 'tests'

BUILD_TEST_ARGS = {
    'host': HOST,  # /vVelvet',
    'port': PORT,
    'test_loader_name': 'apitests',  # do not remove! needed for reporting!
}


def load_tests(loader, tests, pattern):
    """Provide a TestSuite to the discovery process."""
    test_dir = os.path.join(os.path.dirname(__file__), TESTS_DIR)
    return driver.build_tests(test_dir, loader, **BUILD_TEST_ARGS)
