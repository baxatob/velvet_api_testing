
from utils.collections import ADMIN_USER, CLIENT_USER
from utils.tools import (get_random_values2, countdown, get_token, generate_client_data, generate_natural_user_data,
                         get_expected_data, generate_legal_user_data, generate_checklist, set_all_services_enabled_,
                         set_bank_account_service_enabled, set_user_service_enabled, set_escrow_service_enabled,
                         set_blacklist_service_enabled, set_kyc_service_enabled, set_dealtype_service_enabled,
                         set_gdpr_service_enabled, set_gdpr_teplate_)
						 
						 
def set_token(test):
	token = get_token(CLIENT_USER)
	test.test_data['request_headers']['Authorization'] = f'Bearer {token}'


def set_token_admin(test):
    token = get_token(ADMIN_USER)
    test.test_data['request_headers']['Authorization'] = f'Bearer {token}'


def set_client_data(test):
    data = generate_client_data()
    for key in data:
        test.test_data['data'][key] = data[key]


def set_superuser_data(test):
    data = generate_client_data(is_admin=True)
    for key in data:
        test.test_data['data'][key] = data[key]


def set_expected_data(test):
    data = get_expected_data()
    for key in data:
        test.test_data['response_strings'].append(data[key])


def set_random_natural_user(test):
    if not 'blacklist' in test.test_data['name']:
        data = generate_natural_user_data()
    else:
        data = generate_natural_user_data(blacklist=True)
    if 'update' in test.test_data['name']:
        data['Email'] = 'natural@shit.yes'
    for key in data:
        test.test_data['data'][key] = data[key]


def set_random_legal_user(test):
    if not 'blacklist' in test.test_data['name']:
        data = generate_legal_user_data()
    else:
        data = generate_legal_user_data(blacklist=True)
    if 'update' in test.test_data['name']:
        data['Email'] = 'legal@shit.yes'
    for key in data:
        test.test_data['data'][key] = data[key]


def set_checklist(test):
    data = generate_checklist()
    for key in data:
        test.test_data['data'][key] = data[key]


def wait_before_proceed(test):
    # TODO add optional delay settings from yaml
    name = test.test_data['name']
    print('')
    countdown(30, f'Test {name}, preprocess: wait')


def wait_before_proceed_long(test):
    name = test.test_data['name']
    print('')
    countdown(120, f'Test {name}, preprocess: wait')


def set_all_services_enabled(test):
    data = set_all_services_enabled_(flag=True)
    test.test_data['data'] = data


def set_all_services_disabled(test):
    data = set_all_services_enabled_(flag=False)
    test.test_data['data'] = data


def set_bank_account_services_enabled(test):
    data = set_bank_account_service_enabled(flag=True)
    test.test_data['data'] = data


def set_user_services_enabled(test):
    data = set_user_service_enabled(flag=True)
    test.test_data['data'] = data


def set_escrow_services_enabled(test):
    data = set_escrow_service_enabled(flag=True)
    test.test_data['data'] = data


def set_blacklist_services_enabled(test):
    data = set_blacklist_service_enabled(flag=True)
    test.test_data['data'] = data


def set_kyc_services_enabled(test):
    data = set_kyc_service_enabled(flag=True)
    test.test_data['data'] = data


def set_dealtype_services_enabled(test):
    data = set_dealtype_service_enabled(flag=True)
    test.test_data['data'] = data


def set_gdpr_services_enabled(test):
    data = set_gdpr_service_enabled(flag=True)
    test.test_data['data'] = data


def set_gdpr_template(test):
    data = set_gdpr_teplate_(flag=True)
    test.test_data['data'] = data


PREPROCESS = {
	'set_token': set_token,
    'set_token_admin': set_token_admin,
    'set_client_data': set_client_data,
    'set_superuser_data': set_superuser_data,
    'set_expected_data': set_expected_data,
    'set_random_natural_user': set_random_natural_user,
    'set_random_legal_user': set_random_legal_user,
    'set_checklist': set_checklist,
    'wait_before_proceed': wait_before_proceed,
    'wait_before_proceed_long': wait_before_proceed_long,
    'set_all_services_enabled': set_all_services_enabled,
    'set_all_services_disabled': set_all_services_disabled,
    'set_bank_account_services_enabled': set_bank_account_services_enabled,
    'set_user_services_enabled': set_user_services_enabled,
    'set_escrow_services_enabled': set_escrow_services_enabled,
    'set_blacklist_services_enabled': set_blacklist_services_enabled,
    'set_kyc_services_enabled': set_kyc_services_enabled,
    'set_dealtype_services_enabled': set_dealtype_services_enabled,
    'set_gdpr_services_enabled': set_gdpr_services_enabled,
    'set_gdpr_template': set_gdpr_template,
}


POSTPROCESS = {

}


def invoke_handlers(test, pre_or_postprocess):
    func_table = {
        'preprocess': PREPROCESS,
        'postprocess': POSTPROCESS,
    }
    handlers = test.test_data.get(pre_or_postprocess, [])
    actions = handlers if isinstance(handlers, list) else [handlers]
    for action in actions:
        func_table[pre_or_postprocess][action](test)
