import sys
import time
import datetime
import base64
import requests
from faker import Faker
from utils.shareholder import SingletonServant
from utils.collections import get_blacklist_country_code
from utils.configuration import HOST, PORT, MAIL_SERVER, MAIL_USER, MAIL_PSWRD

#BASE_USER_EMAIL = 'jeff@amazon.com'

def countdown(seconds, message, suffix="Done"):
    disp = f'{message}... {seconds} sec(s) remain'
    disp_len = len(disp)
    for i in range(seconds, 0, -1):
        sys.stdout.write(f'{message}... {i} sec(s) remain'.ljust(disp_len))
        sys.stdout.flush()
        sys.stdout.write('\r')
        sys.stdout.flush()
        time.sleep(1)
    print(f'{message}... {suffix}'.ljust(disp_len))


def get_random_values2(length):
    import random
    dic_str = 'QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm'
    dic_num = '1234567890'
    rnd_str = rnd_num = ''
    for i in range(length):
        rnd_chr = dic_str[random.randint(0, len(dic_str) - 1)]
        rnd_dig = dic_num[random.randint(0, len(dic_num) - 1)]
        rnd_str += rnd_chr
        rnd_num += rnd_dig
    return rnd_str, rnd_num


def check_email_for_token2(delete_mail=True):
    import re
    import time
    import poplib
    TOKEN_PATTERN = '=[0-9a-zA-Z=]+'
    timer = 0

    while not timer == 300:
        print('')
        countdown(15, 'Waiting before acessing user mailbox')
        timer += 15  # check mailbox each 15 sec.
        try:
            mailbox = poplib.POP3_SSL(MAIL_SERVER, 995)
            print('Mailbox connected')
        except TimeoutError:
            print('Mail server connection timeout error')
            exit(666)
        mailbox.user(MAIL_USER)
        login = mailbox.pass_(MAIL_PSWRD)
        if 'Logged in' in str(login):
            tot_msg, _ = mailbox.stat()
            for i in range(tot_msg, 0, -1):
                _, msg_line_list, _ = mailbox.retr(i)
                for line in msg_line_list:
                    if 'token' in str(line):
                        token = re.search(TOKEN_PATTERN, str(line)).group(0)
                        if delete_mail:
                            mailbox.dele(i)
                        mailbox.quit()
                        return token[1:]

        else:
            print('Mail server connection error')
            return None
        mailbox.quit()

    print('No token received in 5 minutes')
    return None


def get_token(user_data):
    session = requests.Session()
    token_api = f'http://{HOST}:{PORT}/vVelvet/oauth/token'
    encoded_bytes = base64.b64encode(f'{user_data[0]}:{user_data[1]}'.encode('utf-8'))
    encoded_string = str(encoded_bytes, 'utf-8')
    headers = {'accept':'application/json', 'Authorization':f'Basic {encoded_string}'}
    try:
        request_for_token = session.post(token_api, headers=headers).json()
        token = request_for_token['access_token']
        return token
    except:
        print('GET TOKEN ERROR')


def generate_client_data(is_admin=False):
    f = Faker()
    data = {
        "PlatformURL": f.url(),
        "HeadquartersAddress.AddressLine1": f.street_address(),
        "HeadquartersAddress.AddressLine2": "",
        "HeadquartersAddress.City": f.city(),
        "HeadquartersAddress.PostalCode": f.postalcode(),
        "HeadquartersAddress.Country": 'ru',  # f.country_code().lower(),
        "CompanyReference": f.company(),
        "Name": f.color_name().upper(),
        "RegisteredName": f.color_name().upper(),
        "PlatformDescription": f.text(50),
        "Password": 'shalom',
        "IsSuperuser": False if not is_admin else True
    }
    SingletonServant().set_data(data=data)
    return data


def generate_natural_user_data(blacklist=False):
    f = Faker()
    data = {
          "Email": f.email(),
          "FirstName": f.first_name(),
          "LastName": f.last_name(),
          "Tag": f.text(5),
          "Address.AddressLine1": f.street_address(),
          "Address.AddressLine2": "",
          "Address.City": f.city(),
          "Address.Region": f.country_code().lower(),
          "Address.PostalCode": f.postalcode(),
          "Address.Country": "pl" if not blacklist else get_blacklist_country_code(),
          "CountryOfResidence": "pl",
          "Nationality": f.country_code().lower(),
          "Birthday": f.unix_time(),
          "Occupation": f.text(7),
          "IncomeRange": f.random_int(min=10, max=999)
    }
    return data


def generate_legal_user_data(blacklist=False):
    f = Faker()
    data = {
          "Email": f.email(),
          "Name": f.company(),
          "Tag": f.text(5),
          "HeadquartersAddress.AddressLine1": f.street_address(),
          "HeadquartersAddress.AddressLine2": "",
          "HeadquartersAddress.City": f.city(),
          "HeadquartersAddress.Region": f.country_code().lower(),
          "HeadquartersAddress.PostalCode": f.postalcode(),
          "HeadquartersAddress.Country": "it" if not blacklist else get_blacklist_country_code(),
          "LegalRepresentativeCountryOfResidence": f.country_code().lower(),
          "LegalRepresentativeNationality": f.country_code().lower(),
          "LegalRepresentativeBirthday": f.unix_time(),
          "LegalRepresentativeProofOfIdentity": f.text(5),
          "LegalPersonType": f.text(5),
          "Statute": f.text(5),
          "CompanyNumber": f.isbn10(),
          "LegalRepresentativeEmail": f.email(),
          "LegalRepresentativeFirstName": f.first_name(),
          "LegalRepresentativeLastName": f.last_name(),
          "LegalRepresentativeAddresss.AddressLine1": f.street_address(),
          "LegalRepresentativeAddress.AddressLine2": "",
          "LegalRepresentativeAddress.City": f.city(),
          "LegalRepresentativeAddress.Region": f.country_code().lower(),
          "LegalRepresentativeAddress.PostalCode": f.postalcode(),
          "LegalRepresentativeAddress.Country": f.country_code().lower()
    }
    return data


def generate_checklist():
    f = Faker()
    data = {
              "Checklist": {
                "seller_id": 2,
                "buyer_id": 1,
                "tag": "Cosy House offer id 100500 by BaitHaim Co.",
                "subject": "Checklist for testing",
                "language": "de",
                "template_id": 1,
                "amount": 50000000,
                "currency": "EUR",
                "expiration_date": 1577707200
              },
              "Items": [
                {
                  "user_id": 2,
                  "sequence_no": 1,
                  "task_description": "Submit official offer to Escrow house",
                  "amount_due": 0,
                  "currency": "BYR",
                  "due_date": 1577707200
                },
                {
                  "user_id": 1,
                  "sequence_no": 2,
                  "task_description": "Provide the escrow company with your earnest money deposit",
                  "amount_due": 1000000,
                  "currency": "EUR",
                  "due_date": 1577707200
                },
                {
                  "user_id": 1,
                  "sequence_no": 3,
                  "task_description": "Schedule and complete a general home inspection",
                  "amount_due": 0,
                  "currency": "",
                  "due_date": 1577707200
                }
              ]
            }
    return data


def set_all_services_enabled_(flag):
    data = [
    {
        "OperationId": "create_bank_account",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "update_bank_account",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "list_bank_accounts",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "create_natural_user",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "update_natural_user",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "create_legal_user",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "update_legal_user",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "view_user",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "view_users",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "get_status_listing_templates",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "preview_gdpr_document",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "create_escrow_account",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "update_escrow_account",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "create_escrow_checklist",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "show_paper_contract",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "create_kyc_document",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "create_kyc_page",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "view_kyc_document",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "view_kyc_document_for_user",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "view_kyc_document_for_client",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "view_kyc_page_by_id",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "show_kyc_pages",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "submit_kyc_document",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "upload_user_photo",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "get_actual_blacklist",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "list_all_deal_types",
        "IsEnabled": True if flag else False
    }
    ]
    return data


def set_bank_account_service_enabled(flag):
    data = [
    {
        "OperationId": "create_bank_account",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "update_bank_account",
        "IsEnabled": True if flag else False
    },
    {
        "OperationId": "list_bank_accounts",
        "IsEnabled": True if flag else False
    }
    ]
    return data


def set_user_service_enabled(flag):
    data = [
        {
            "OperationId": "create_natural_user",
            "IsEnabled": True if flag else False
        },
        {
            "OperationId": "update_natural_user",
            "IsEnabled": True if flag else False
        },
        {
            "OperationId": "create_legal_user",
            "IsEnabled": True if flag else False
        },
        {
            "OperationId": "update_legal_user",
            "IsEnabled": True if flag else False
        },
        {
            "OperationId": "view_user",
            "IsEnabled": True if flag else False
        },
        {
            "OperationId": "view_users",
            "IsEnabled": True if flag else False
        }
    ]
    return data


def set_escrow_service_enabled(flag):
    data = [
        {
            "OperationId": "create_escrow_account",
            "IsEnabled": True if flag else False
        },
        {
            "OperationId": "update_escrow_account",
            "IsEnabled": True if flag else False
        },
        {
            "OperationId": "create_escrow_checklist",
            "IsEnabled": True if flag else False
        }
    ]
    return data


def set_blacklist_service_enabled(flag):
    data = [
        {
            "OperationId": "get_actual_blacklist",
            "IsEnabled": True if flag else False
        }
    ]
    return data


def set_kyc_service_enabled(flag):
    data = [
        {
            "OperationId": "create_kyc_document",
            "IsEnabled": True if flag else False
        },
        {
            "OperationId": "create_kyc_page",
            "IsEnabled": True if flag else False
        },
        {
            "OperationId": "view_kyc_document",
            "IsEnabled": True if flag else False
        },
        {
            "OperationId": "view_kyc_document_for_user",
            "IsEnabled": True if flag else False
        },
        {
            "OperationId": "view_kyc_document_for_client",
            "IsEnabled": True if flag else False
        },
        {
            "OperationId": "view_kyc_page_by_id",
            "IsEnabled": True if flag else False
        },
        {
            "OperationId": "show_kyc_pages",
            "IsEnabled": True if flag else False
        },
        {
            "OperationId": "upload_user_photo",
            "IsEnabled": True if flag else False
        },
        {
            "OperationId": "submit_kyc_document",
            "IsEnabled": True if flag else False
        },
        {
            "OperationId": "view_user",
            "IsEnabled": True if flag else False
        }
    ]
    return data


def set_dealtype_service_enabled(flag):
    data = [
        {
            "OperationId": "list_all_deal_types",
            "IsEnabled": True if flag else False
        }
    ]
    return data


def set_gdpr_service_enabled(flag):
    data = [
        {
            "OperationId": "get_status_listing_templates",
            "IsEnabled": True if flag else False
        },
        {
            "OperationId": "preview_gdpr_document",
            "IsEnabled": True if flag else False
        }
    ]
    return data


def set_gdpr_teplate_(flag):
    data = [
        {
            "DocumentReferenceNo": 1010,
            "Language": "en",
            "IsEnabled": True if flag else False
        }
    ]
    return data


def get_expected_data():
    return SingletonServant().get_data()