

ADMIN_USER = 'putin@sntozero.com', 'shalom'
CLIENT_USER = 'jeff@amazon.com', 'shalom'

COUNTRY_BLACKLIST = [
      {
        "CountryName": "Afghanistan",
        "Country": "af"
      },
      {
        "CountryName": "Bahamas",
        "Country": "bs"
      },
      {
        "CountryName": "Bosnia and Herzegovina",
        "Country": "ba"
      },
      {
        "CountryName": "Botswana",
        "Country": "bw"
      },
      {
        "CountryName": "Cambodia",
        "Country": "kh"
      },
      {
        "CountryName": "Ethiopia",
        "Country": "et"
      },
      {
        "CountryName": "Ghana",
        "Country": "gh"
      },
      {
        "CountryName": "Guyana",
        "Country": "gy"
      },
      {
        "CountryName": "Iran (Islamic Republic of)",
        "Country": "ir"
      },
      {
        "CountryName": "Iraq",
        "Country": "iq"
      },
      {
        "CountryName": "Korea (Democratic People's Republic of)",
        "Country": "kp"
      },
      {
        "CountryName": "Lao People's Democratic Republic",
        "Country": "la"
      },
      {
        "CountryName": "Pakistan",
        "Country": "pk"
      },
      {
        "CountryName": "Serbia",
        "Country": "rs"
      },
      {
        "CountryName": "Sri Lanka",
        "Country": "lk"
      },
      {
        "CountryName": "Syrian Arab Republic",
        "Country": "sy"
      },
      {
        "CountryName": "Trinidad and Tobago",
        "Country": "tt"
      },
      {
        "CountryName": "Tunisia",
        "Country": "tn"
      },
      {
        "CountryName": "Uganda",
        "Country": "ug"
      },
      {
        "CountryName": "Vanuatu",
        "Country": "vu"
      },
      {
        "CountryName": "Yemen",
        "Country": "ye"
      }
    ]


def get_blacklist_country_code():
    import random
    return random.choice( [country['Country'] for country in COUNTRY_BLACKLIST] )