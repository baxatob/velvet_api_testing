

class SingletonServant:
    '''Singleton class to store and share data during testrun'''
    instance = None
    zanachka = None

    def __new__(cls):
        if cls.instance is None:
            cls.instance = super().__new__(cls)
        return cls.instance

    def set_data(self, data):
        self.zanachka = data

    def set_data_as_strings(self, data):
        self.zanachka = [data[key] for key in data]

    def get_data(self):
        return self.zanachka