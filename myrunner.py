"""
A TestRunner for use with the Python unit testing framework. It
generates a tabular report to show the result at a glance.

"""

# __package__ = "test_development"

import os
import datetime
import sys
import shutil
from functools import reduce
from itertools import dropwhile
import unittest
from utils.prettytable import PrettyTable
from utils.shareholder import SingletonServant
# from handlers import invoke_handlers


TestResult = unittest.TestResult


class _TestResult(TestResult):
    def __init__(self, verbosity=1):
        TestResult.__init__(self)
        self.tests_count = 0  # total amount of tests
        self.success_count = 0
        self.failure_count = 0
        self.error_count = 0
        self.skip_count = 0
        self.verbosity = verbosity

        # result is a list of result in 4 tuple
        # (
        #   result code (0: success; 1: fail; 2: error),
        #   TestCase object,
        #   Test output (byte string),
        #   stack trace,
        # )
        self.result = []

    def startTest(self, test):
        # transfer tests metadata
        prior_meta = test.prior.test_data.get('meta', {}) if test.prior else {}
        current_meta = test.test_data.get('meta', {})
        current_meta.update(prior_meta)
        test.test_data['meta'] = current_meta
        TestResult.startTest(self, test)

    def stopTest(self, test):
        TestResult.stopTest(self, test)

    def addSuccess(self, test):
        self.success_count += 1
        TestResult.addSuccess(self, test)
        self.result.append((0, test, '', ''))
        if self.verbosity > 1:
            sys.stderr.write('ok ')
            sys.stderr.write(str(test))
            sys.stderr.write('\n')
        else:
            sys.stderr.write('.')
            sys.stderr.flush()

    def addError(self, test, err):
        self.error_count += 1
        TestResult.addError(self, test, err)
        _, _exc_str = self.errors[-1]
        self.result.append((2, test, '', _exc_str))
        if self.verbosity > 1:
            sys.stderr.write('E  ')
            sys.stderr.write(str(test))
            sys.stderr.write('\n')
        else:
            sys.stderr.write('E')
            sys.stderr.flush()

    def addFailure(self, test, err):
        self.failure_count += 1
        TestResult.addFailure(self, test, err)
        _, _exc_str = self.failures[-1]
        self.result.append((1, test, '', _exc_str))
        if self.verbosity > 1:
            sys.stderr.write('F  ')
            sys.stderr.write(str(test))
            sys.stderr.write('\n')
        else:
            sys.stderr.write('F')
            sys.stderr.flush()

    def addSkip(self, test, reason):
        self.skip_count += 1
        TestResult.addSkip(self, test, reason)
        _, reason = self.skipped[-1]
        self.result.append((3, test, '', reason))
        if self.verbosity > 1:
            sys.stderr.write('S  ')
            sys.stderr.write(str(test))
            sys.stderr.write('\n')
        else:
            sys.stderr.write('S')
            sys.stderr.flush()


class TestRunner:
    """
    """
    def __init__(self, stream=sys.stdout, verbosity=1, title=None,
                 description=None):
        self.verbosity = verbosity
        self.startTime = datetime.datetime.utcnow()

    def run(self, test):
        "Run the given test case or test suite."
        result = _TestResult(self.verbosity)
        result.tests_count = test.countTestCases()
        print(
            (f"\nFound {result.tests_count} tests, "
             f"started at {self.startTime.strftime('%c')} (UTC)")
        )
        test(result)
        self.stopTime = datetime.datetime.utcnow()
        dt = self.stopTime - self.startTime
        hours, remainder = divmod(dt.total_seconds(), 3600)
        minutes, seconds = divmod(remainder, 60)
        duration = (
            f"{'0' if hours < 10 else ''}{int(hours)}:"
            f"{'0' if minutes < 10 else ''}{int(minutes)}:"
            f"{'0' if seconds < 10 else ''}{int(seconds)}"
        )
        print(f"\nduration {duration}")
        self.generate_report(result)
        return result

    def generate_report(self, result):
        # Declarations
        term_width = shutil.get_terminal_size()[0]
        # term_width = 200
        names1 = [
                 "No",
                 "Folder",
                 "Declaration from",
                 "Test Name",
                 "Status",
        ]
        summary_table = PrettyTable(
            names1,
            align="l",
            max_table_width=term_width - len(names1) - 1
        )
        summary_table.min_width = dict(map(lambda x: (x, len(x)), names1))

        names2 = ["No", "Test Name", "Reason for skip"]
        skip_table = PrettyTable(
            names2,
            align="l",
            max_table_width=term_width - len(names2) - 1
        )
        skip_table.min_width = dict(map(lambda x: (x, len(x)), names2))

        names3 = ["No", "Test Name", "Assertion failed"]
        fail_table = PrettyTable(
            names3,
            align="l",
            max_table_width=term_width - len(names3) - 1
        )
        fail_table.min_width = dict(map(lambda x: (x, len(x)), names3))

        statuses = {
            0: {'name': 'passed', },
            1: {'name': 'FAIL',
                'report_func': lambda args: fail_table.add_row(
                    reduce(
                        lambda acc, x: [*acc, x[1]] if
                        x[0] in ['counter', 'name', 'error'] else
                        acc, args, []
                           )
                                                                ),
                },
            2: {'name': 'ERROR',
                # programming errors would be printed immediately
                'report_func': lambda args: print(
                    reduce(lambda acc, x: [*acc, x[1]] if
                           x[0] in ['error'] else acc, args, []
                           )
                                                  )
                },
            3: {'name': 'SKIP',
                'report_func': lambda args: skip_table.add_row(
                    reduce(
                           lambda acc, x: [*acc, x[1]] if
                           x[0] in ['counter', 'name', 'skip'] else
                           acc, args, []
                           )
                                                               ),
                },
        }

        # printing summary
        summary = (f"\nsummary (total {result.tests_count}, "
                   f"skipped {result.skip_count}, "
                   f"passed {result.success_count}, "
                   f"failed {result.failure_count}, "
                   f"errors {result.error_count}):")
        print(summary)

        # collecting results info
        for i, (n, test, output, error) in enumerate(result.result):
            status = statuses.get(n, {})
            testClass = test.__class__

            report_args = [
                ('counter', i + 1),
                ('folder', os.path.basename(
                    os.path.normpath(testClass.test_directory))
                 ),
                ('file', f"{testClass.__name__.split('_')[0]}.yaml"),
                ('name', testClass.test_data['name']),
                ('status', status.get('name', 'undefined')),
                ('skip', testClass.test_data['skip']),
                ('output', output),
                ('error',
                 "\n".join([*filter(None,
                                    dropwhile(lambda x: "Error" not in x,
                                              error.split("\n"))
                                    )
                            ][:5]
                           ).replace('\'', '')
                 ),
            ]

            # fulfill totals table
            args = reduce(lambda acc, x: [*acc, x[1]]
                          if x[0] in
                          ['counter', 'folder', 'file', 'name', 'status'] else
                          acc,
                          report_args, []
                          )
            summary_table.add_row(args)

            # fulfill other reports (if function declared)
            status.get('report_func', lambda x: x)(report_args)

        # print summary table
        print(summary_table.get_string())

        # print skipped table
        if result.skip_count:
            print('\nSkipped tests list:')
            print(skip_table.get_string())

        # print failed table
        if result.failure_count:
            print('\nFailed tests:')
            print(fail_table.get_string())


##############################################################################
# Facilities for running tests from the command line
##############################################################################

# Note: Reuse unittest.TestProgram to launch test. In the future we may
# build our own launcher to support more specific command line
# parameters like test title, CSS, etc.
class TestProgram(unittest.TestProgram):
    """
    A variation of the unittest.TestProgram. Please refer to the base
    class for command line parameters.
    """
    def runTests(self):
        # Pick TestRunner as the default test runner.
        # base class's testRunner parameter is not useful because it means
        # we have to instantiate TestRunner before we know self.verbosity.
        if self.testRunner is None:
            self.testRunner = TestRunner(verbosity=self.verbosity)
        unittest.TestProgram.runTests(self)


main = TestProgram

##############################################################################
# Executing this module from the command line
##############################################################################

if __name__ == "__main__":
    shareholder = SingletonServant()
    main(module=None)
